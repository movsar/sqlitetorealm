package biz.mrexclusive.sqlitetorealm;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.lang.reflect.Array;
import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.Ignore;

enum LG_CODES {
    ENG, ARA, BEN, KAT, INH, SPA, XAL, ZHO, KOR, LEZ, DEU, NLD, NOR, FAS, POR, RUS, TAT, UZB, FRA, HIN, CHE, JPN, LAT; //EXACT MATCH WITH ARRAY
}


public class MainActivity extends AppCompatActivity {
    static SQLiteDatabase db;
    static cDBHelper dbhelper;
    static final String LOG_TAG = "SqliteToRealm";

    int count;
    Cursor pub_cursor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        if (dbhelper == null) {
            dbhelper = new cDBHelper(MainActivity.this);
            dbhelper.createDataBase();
            dbhelper.openDataBase();
            db = dbhelper.getDB();
        }

        RealmConfiguration realmConfig = new RealmConfiguration.Builder(MainActivity.this).build();
        Realm.setDefaultConfiguration(realmConfig);
        Realm realm = Realm.getDefaultInstance();

        final RealmResults<xPhrase> pFromDb = realm.where(xPhrase.class).findAll();
        for (int i = 0; i < pFromDb.size(); i++) {
            Log.d("123", i + " : " + pFromDb.get(i).phrase);
        }





        Thread ast = new Thread() {
            @Override
            public void run() {
                RealmConfiguration realmConfig = new RealmConfiguration.Builder(MainActivity.this).build();
                Realm.setDefaultConfiguration(realmConfig);
                Realm realm = Realm.getDefaultInstance();

                //******************************* PHRASES **********************************
                Log.d(LOG_TAG, "PROCESSING PHRASES");

                String query = "SELECT * FROM " + "phrases";
                pub_cursor = db.rawQuery(query, null);
                ArrayList<xPhrase> phrases = new ArrayList<xPhrase>();
                count = pub_cursor.getCount();
                while (pub_cursor.moveToNext()) {
                    xPhrase xp = new xPhrase(
                            pub_cursor.getString(0),
                            pub_cursor.getString(1),
                            pub_cursor.getString(2),
                            pub_cursor.getString(3),
                            pub_cursor.getString(4),
                            pub_cursor.getString(5),
                            pub_cursor.getString(6),
                            pub_cursor.getString(7),
                            pub_cursor.getString(8),
                            pub_cursor.getString(9),
                            pub_cursor.getString(10)
                    );
                    phrases.add(xp);
                }

                long time = System.currentTimeMillis();

                realm.beginTransaction();
                for (int i = 0; i < phrases.size(); i++) {
                    final xPhrase xpp = realm.copyToRealm(phrases.get(i)); // Persist unmanaged objects
                }
                realm.commitTransaction();

                time = System.currentTimeMillis() - time;
                Log.d(LOG_TAG, count + " has been committed successfully");
                Log.d(LOG_TAG, "elapsed-time:" + String.valueOf(time) + "ms.");

                //***************************** TRANSLATIONS ******************************
                Log.d(LOG_TAG, "PROCESSING TRANSLATIONS");


                query = "SELECT * FROM " + "translations";
                pub_cursor = db.rawQuery(query, null);
                ArrayList<xTranslation> translations = new ArrayList<xTranslation>();
                count = pub_cursor.getCount();
                while (pub_cursor.moveToNext()) {
                    xTranslation xt = new xTranslation(
                            pub_cursor.getString(0),
                            pub_cursor.getString(1),
                            pub_cursor.getString(2),
                            pub_cursor.getString(3),
                            pub_cursor.getString(4),
                            pub_cursor.getString(5),
                            pub_cursor.getString(6)
                    );
                    translations.add(xt);
                }

                time = System.currentTimeMillis();

                realm.beginTransaction();
                for (int i = 0; i < translations.size(); i++) {
                    final xTranslation xtt = realm.copyToRealm(translations.get(i)); // Persist unmanaged objects
                }
                realm.commitTransaction();

                time = System.currentTimeMillis() - time;
                Log.d(LOG_TAG, count + " has been committed successfully");
                Log.d(LOG_TAG, "elapsed-time:" + String.valueOf(time) + "ms.");


                //***************************** RUNOPHRASES ******************************
                Log.d(LOG_TAG, "PROCESSING RUNOPHRASES");


                query = "SELECT * FROM " + "userphrases";
                pub_cursor = db.rawQuery(query, null);
                ArrayList<xRnPhrase> rnps = new ArrayList<xRnPhrase>();
                count = pub_cursor.getCount();
                while (pub_cursor.moveToNext()) {
                    xRnPhrase rnp = new xRnPhrase(
                            pub_cursor.getString(0),
                            pub_cursor.getString(1),
                            pub_cursor.getString(2)
                    );
                    rnps.add(rnp);
                }

                time = System.currentTimeMillis();

                realm.beginTransaction();
                for (int i = 0; i < rnps.size(); i++) {
                    final xRnPhrase rnpp = realm.copyToRealm(rnps.get(i)); // Persist unmanaged objects
                }
                realm.commitTransaction();

                time = System.currentTimeMillis() - time;
                Log.d(LOG_TAG, count + " has been committed successfully");
                Log.d(LOG_TAG, "elapsed-time:" + String.valueOf(time) + "ms.");
            }
        };

        // ast.run();
    }


}
