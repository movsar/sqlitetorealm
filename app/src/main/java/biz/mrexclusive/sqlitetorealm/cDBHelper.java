package biz.mrexclusive.sqlitetorealm;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

public class cDBHelper extends SQLiteOpenHelper {
    private static final String TAG = "DBHelper";
    private static String DB_DIR = "";
    private static String DB_FILENAME = "nohchiyn.sdb";
    //The Android's default system path of your application database.


    private SQLiteDatabase _db;
    private final Context _cxt;

    /**
     * Constructor
     * Takes and keeps a reference of the passed context in order to access to the application assets and resources.
     *
     * @param context
     */
    public cDBHelper(Context context) {
        super(context, DB_FILENAME, null, 1);
        _db = null;
        _cxt = context;
        DB_DIR = _cxt.getFilesDir() + "/";
    }

    public SQLiteDatabase getDB() {
        return _db;
    }

    /**
     * Creates an empty database on the system and rewrites it with your own database.
     */
    public void createDataBase() {
        boolean dbExist = checkDataBase();

        if (dbExist)
            return;

        // By calling this method and empty database will be created into the default system path
        // of your application so we are gonna be able to overwrite that database with our database.
        this.getReadableDatabase();
        copy_files(R.raw.nohchiyn, DB_DIR + DB_FILENAME);
        //for working with zip audio source
      /*  final File _path = new File(_cxt.getFilesDir() + "/tts/");
        // Unpacking the sound files
        if (!_path.exists()) {
            try {
                _path.mkdir();
                myBaseActivity.unpackZip(_cxt.getAssets().open("tts.zip"), _path.getAbsolutePath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }*/
    }

    /**
     * Check if the database already exist to avoid re-copying the file each time you open the application.
     *
     * @return true if it exists, false if it doesn't
     */
    private boolean checkDataBase() {
        SQLiteDatabase checkDB = null;
        Log.i(TAG, "Checking DB...");
        try {
            String path = DB_DIR + DB_FILENAME;
            File fex = new File(DB_DIR + DB_FILENAME);

            if (!fex.exists()) {
                return false;
            } else {
                checkDB = SQLiteDatabase.openDatabase(DB_DIR + DB_FILENAME, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.CREATE_IF_NECESSARY);
            }
            Log.i(TAG, "Database already exists.");
        } catch (SQLiteException e) {
            Log.i(TAG, "Database does not exist yet.");
        }
        if (checkDB != null) {
            checkDB.close();
        }
        return checkDB != null;
    }

    /**
     * Copies your database from your local assets-folder to the just created empty database in the
     * system folder, from where it can be accessed and handled.
     * This is done by transfering bytestream.
     */

    private void copy_files(int filename, String dstFileName) {
        try {
            File directory = new File(DB_DIR);
            directory.mkdirs();

            InputStream in;
            OutputStream out;
            File outFile = new File(dstFileName);
            in = _cxt.getResources().openRawResource(filename);
            out = new FileOutputStream(outFile);
            byte[] buffer = new byte[32 * 1024]; // play with sizes..
            int readCount;
            while ((readCount = in.read(buffer)) != -1) {
                out.write(buffer, 0, readCount);
            }
             out.flush();
            out.close();
            in.close();

            Log.d("123", "copyed!");
            Log.d("123", "exists!");


        } catch (FileNotFoundException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }


    public void openDataBase() throws SQLException {
        if (null != _db)
            return;
        _db = SQLiteDatabase.openDatabase(DB_DIR + DB_FILENAME, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.CREATE_IF_NECESSARY);
    }

    @Override
    public synchronized void close() {
        if (_db != null)
            _db.close();
        _db = null;
        super.close();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }


    private String read_line_from_destination(String filename) throws IOException {
        File outFile = new File(filename);
        if (!outFile.exists()) return "0";
        FileInputStream fin = new FileInputStream(outFile);
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fin, "UTF-8"));
        String line = bufferedReader.readLine();
        bufferedReader.close();
        return line;
    }
}
