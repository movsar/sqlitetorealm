package biz.mrexclusive.sqlitetorealm;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Movsar Bekaev on 30.05.2016.
 */
public class xRnPhrase extends RealmObject {
    // Key
    @PrimaryKey
    protected int id;

    // Russian
    protected String phrase;

    // Chechen
    protected String translation;

    public xRnPhrase(){}
    public xRnPhrase(String id, String phrase, String translation){
        this.id=Integer.parseInt(id);
        this.phrase=phrase;
        this.translation=translation;
    }
}