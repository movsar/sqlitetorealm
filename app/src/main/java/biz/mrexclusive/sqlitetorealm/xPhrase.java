package biz.mrexclusive.sqlitetorealm;

import java.util.ArrayList;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Movsar Bekaev on 30.05.2016.
 */
public class xPhrase extends RealmObject {
    @PrimaryKey
    protected int id;

    protected int uid, cid, rate;
    protected String phrase, forms, date, type, source, notes, audiopath;

    @Ignore
    protected ArrayList<xTranslation> translations;

    public xPhrase() {
        translations = new ArrayList<xTranslation>();
    }

    public xPhrase(String pid,String uid, String phrase, String forms, String type, String cid, String audio, String source,  String rate, String notes, String date) {

        this.id=Integer.parseInt(pid);
        this.uid=Integer.parseInt(uid);
        this.phrase=phrase;
        this.forms=forms;
        this.type=type;
        this.cid=Integer.parseInt(cid);
        this.source=source;
        this.audiopath=audio;
        this.rate=Integer.parseInt(rate);
        this.notes=notes;
        this.date=date;

        translations = new ArrayList<xTranslation>();
    }

}