package biz.mrexclusive.sqlitetorealm;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Movsar Bekaev on 30.05.2016.
 */
public class xTranslation extends RealmObject {
    @PrimaryKey
    protected int id = -1;
    protected int pid = -1;
    protected int uid = -1;

    protected String translation;
    protected String lang;
    protected int rate = 0;
    protected String date = "";
    protected String notes = "";


    public xTranslation() {
    }


    public xTranslation(String tid, String pid, String uid, String translation, String lang, String rate, String date) {
        this.id = Integer.parseInt(tid);
        this.pid = Integer.parseInt(pid);
        this.uid = Integer.parseInt(uid);
        this.translation = translation;
        this.lang = lang;
        this.rate = Integer.parseInt(rate);
        this.date = date;
    }

}